//main.c

#include <stdio.h>
#include <iostream> //Изменение
#include "calculate.h"

int
main (void)
{
  float Numeral;
  char Operation[4];
  float Result;
  printf("Число: ");
  scanf("%f",&Numeral);
  printf("Операция (+, -, *, /, pow, sqrt, sin, cos, tan): ");
  scanf("%4s", Operation);
  Result = Calculate(Numeral, Operation);
  printf("%6.2f\n",Result);
  return 0;
}
